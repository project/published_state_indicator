<?php

namespace Drupal\published_state_indicator\EventSubscriber;

use Drupal\Core\Config\ConfigEvents;
use Drupal\Core\Config\ConfigImporterEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Published state indicator event subscriber.
 */
class ConfigImportSubscriber implements EventSubscriberInterface {

  /**
   * The published_state_indicator.css_generator service.
   *
   * @var Drupal\published_state_indicator\CssGenerator
   */
  protected $cssGenerator;

  /**
   * Constructs a ConfigImportSubscriber object.
   *
   * @param Drupal\published_state_indicator\CssGenerator $css_generator
   *   The published_state_indicator.css_generator service.
   */
  public function __construct($css_generator) {
    $this->cssGenerator = $css_generator;
  }

  /**
   * The storage is transformed for importing.
   *
   * @param \Drupal\Core\Config\ConfigImporterEvent $event
   *   The config storage transform event.
   */
  public function onImport(ConfigImporterEvent $event) {
    $changes = $event->getChangelist();

    if (in_array('published_state_indicator.settings', $changes['create']) || in_array('published_state_indicator.settings', $changes['update'])) {
      $this->cssGenerator->generateCssFile();
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      ConfigEvents::IMPORT => ['onImport'],
    ];
  }

}
