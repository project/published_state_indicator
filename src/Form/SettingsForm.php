<?php

namespace Drupal\published_state_indicator\Form;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Published State Indicator settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  const DEFAULT_COLOR_PRIMARY = '#6B728E';
  const DEFAULT_COLOR_SECONDARY = '#F3F4FE';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'published_state_indicator_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['published_state_indicator.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['font_size'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Font size'),
      '#size' => 8,
      '#required' => TRUE,
      '#default_value' => $this->config('published_state_indicator.settings')->get('font_size'),
      '#description' => $this->t('The size of the label font e.g. 1rem'),
    ];

    if (\Drupal::service('module_handler')->moduleExists('workflows')) {
      $workflows = \Drupal::entityTypeManager()->getStorage('workflow')->loadMultiple();

      $workflows_config = $this->config('published_state_indicator.settings')->get('workflows');

      foreach ($workflows as $workflow) {
        $states = $workflow->get('type_settings')['states'];

        $form[$workflow->id()] = [
          '#type' => 'details',
          '#title' => $this->t("@workflow workflow", ['@workflow' => $workflow->label()]),
          '#open' => count($workflows) == 1,
        ];

        foreach ($states as $state_id => $state) {
          $form[$workflow->id()][$state_id] = [
            '#type' => 'fieldgroup',
            '#title' => $state['label'],
            '#attributes' => [
              'class' => ['container-inline'],
            ]
          ];

          $form[$workflow->id()][$state_id]['color_primary--' . $workflow->id() . '--' . $state_id] = [
            '#type' => 'color',
            '#title' => $this->t('Primary color'),
            '#default_value' => $workflows_config[$workflow->id()][$state_id]['color_primary'] ?? self::DEFAULT_COLOR_PRIMARY,
            '#attributes' => [
              'class' => ['container-inline'],
            ]
          ];
          $form[$workflow->id()][$state_id]['color_secondary--' . $workflow->id() . '--' . $state_id] = [
            '#type' => 'color',
            '#title' => $this->t('Secondary color'),
            '#default_value' => $workflows_config[$workflow->id()][$state_id]['color_secondary'] ?? self::DEFAULT_COLOR_SECONDARY,
            '#attributes' => [
              'class' => ['container-inline'],
            ]
          ];

        }
      }
    }
    else {
      $form['unpublished'] = [
        '#type' => 'fieldgroup',
        '#title' => $this->t('Unpublished'),
        '#attributes' => [
          'class' => ['container-inline'],
        ]
      ];

      $form['unpublished']['color_primary--core--unpublished'] = [
        '#type' => 'color',
        '#title' => $this->t('Primary color'),
        '#default_value' => $workflows_config['core']['unpublished']['color_primary'] ?? self::DEFAULT_COLOR_PRIMARY,
        '#attributes' => [
          'class' => ['container-inline'],
        ]
      ];

      $form['unpublished']['color_secondary--core--unpublished'] = [
        '#type' => 'color',
        '#title' => $this->t('Secondary color'),
        '#default_value' => $workflows_config['core']['unpublished']['color_secondary'] ?? self::DEFAULT_COLOR_SECONDARY,
        '#attributes' => [
          'class' => ['container-inline'],
        ]
      ];
    }

    $form['#attached']['library'][] = 'published_state_indicator/published_state_indicator.admin';

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $vals = $form_state->getValues();

    // Extract and build array consisting of workflows->states->settings.
    $color_settings = array_filter($vals, function ($key) {
      return str_starts_with($key, 'color_');
    }, ARRAY_FILTER_USE_KEY);

    foreach ($color_settings as $name => $color) {
      $keys = explode('--', $name);
      $colour_config[$keys[1]][$keys[2]][$keys[0]] = $color;
    }

    $this->config('published_state_indicator.settings')
      ->set('workflows', $colour_config)
      ->save();

    $this->config('published_state_indicator.settings')
      ->set('font_size', $vals['font_size'])
      ->save();

    \Drupal::service('published_state_indicator.css_generator')->generateCssFile();

    parent::submitForm($form, $form_state);
  }

}
