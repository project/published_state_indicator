<?php

declare(strict_types=1);

namespace Drupal\published_state_indicator\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\Exception\UndefinedLinkTemplateException;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'Label &amp; Published state' formatter.
 *
 * @FieldFormatter(
 *   id = "published_state_indicator",
 *   label = @Translation("Label &amp; Published state"),
 *   field_types = {"entity_reference"},
 * )
 */
final class PublishedStateIndicatorFormatter extends EntityReferenceFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'link' => TRUE,
      'display_published_label' => FALSE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements['link'] = [
      '#title' => $this->t('Link label to the referenced entity.'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('link'),
    ];

    $elements['display_published_label'] = [
      '#title' => $this->t('Display label for published content.'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('display_published_label'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->getSetting('link') ? $this->t('Link to the referenced entity') : $this->t('No link');
    $summary[] = $this->getSetting('display_published_label') ? $this->t('Display label for published content') : $this->t("Don't display label for published content");
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];
    $output_as_link = $this->getSetting('link');
    $output_as_link = TRUE;

    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $entity) {
      $label = $entity->label();
      // If the link is to be displayed and the entity has a uri, display a
      // link.
      if ($output_as_link && !$entity->isNew()) {
        try {
          $uri = $entity->toUrl();
        }
        catch (UndefinedLinkTemplateException $e) {
          // This exception is thrown by \Drupal\Core\Entity\Entity::urlInfo()
          // and it means that the entity type doesn't have a link template nor
          // a valid "uri_callback", so don't bother trying to output a link for
          // the rest of the referenced entities.
          $output_as_link = FALSE;
        }
      }

      if ($output_as_link && isset($uri) && !$entity->isNew()) {
        $elements[$delta] = [
          '#type' => 'link',
          '#title' => $label,
          '#url' => $uri,
          '#options' => $uri->getOptions(),
        ];

        if (!empty($items[$delta]->_attributes)) {
          $elements[$delta]['#options'] += ['attributes' => []];
          $elements[$delta]['#options']['attributes'] += $items[$delta]->_attributes;
          // Unset field item attributes since they have been included in the
          // formatter output and shouldn't be rendered in the field template.
          unset($items[$delta]->_attributes);
        }

        // PSI tweaks starts here.
        $display_published = $this->getSetting('display_published_label');

        if ($display_published || ($display_published == FALSE && !$entity->isPublished())) {
          $state = 'unpublished';

          if ($entity->hasField('moderation_state')) {
            $state = $entity->get('moderation_state')->getString() ?? $state;

            $workflows = \Drupal::entityTypeManager()->getStorage('workflow')->loadMultiple();

            foreach ($workflows as $workflow) {
              $type_settings = $workflow->get('type_settings');

              // Determine if the referenced entity and bundle is applied to the current workflow.
              if (array_key_exists($entity->getEntityTypeId(), $type_settings['entity_types']) && in_array($entity->bundle(), $type_settings['entity_types'][$entity->getEntityTypeId()]) !== FALSE) {
                $elements[$delta]['#suffix'] = $this->createStatusLabel($workflow->id(), $state);
                $elements[$delta]['#attached']['library'][] = 'published_state_indicator/published_state_indicator.labels';
              }
            }
          }
          else {
            $elements[$delta]['#suffix'] = $this->createStatusLabel('core', $state);
          }
        }

      }
      else {
        $elements[$delta] = ['#plain_text' => $label];
      }
      $elements[$delta]['#entity'] = $entity;
      $elements[$delta]['#cache']['tags'] = $entity->getCacheTags();
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity) {
    return $entity->access('view label', NULL, TRUE);
  }

  /**
   * Generates a label for the given workflow and state.
   *
   * @param string $entity_workflow
   *   The workflow ID.
   * @param string $state
   *   The state ID.
   *
   * @return string
   *   HTML span element denoting the workflow state.
   */
  protected function createStatusLabel($entity_workflow, $state) {
    $entity_workflow = str_replace('_', '-', $entity_workflow);
    $state = str_replace('_', '-', $state);
    $title = ucfirst(str_replace('-', ' ', $state));

    if (empty($entity_workflow)) {
      return ' <span title="Moderation status" class="psi--' . $state . '">' . $title . '</span>';
    }
    else {
      return ' <span title="Moderation status" class="psi--' . $entity_workflow . '--' . $state . '">' . $title . '</span>';
    }

  }

}
