## INTRODUCTION

This module provides a field formatter to display the published/moderation state of the given entity. This is particularly useful in sites that use complex workflows or need quick visual indicators of content status directly in entity displays.

The primary use case for this module is:

- Allow editors to see the published state on entity that are assigned to an entity reference list.

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION
Label colors and font size can be customized using the form under Configuration -> User Interface -> Published State Indicator.

Under 'Manage display' for an entity reference list field select 'Label & Published state' from the available format options.

## CUSTOMIZATION

To customize the labels beyond the text and label colors you can override the classes with your own CSS.

### All labels

Altering the base label CSS can be achieved by adding the following CSS selector to your theme

```css
span[class^="psi--"] {

}
```

### Specific label

To alter Workflow (Workflows module) labels will consist of a class called psi--[workflow_id]--[state_id].
If you are not using Workflows the classes for Core will be called psi--core--unpublished

For example, the following selector could be used to alter a label for the 'draft' state of the 'editorial' workflow:
```css
span[class="psi--editorial--draft"] {

}
```

## MAINTAINERS

Current maintainers for Drupal 10:

- Stephen Cousins (OMAHM) - https://www.drupal.org/u/omahm
